var express = require('express');
var colors = require('colors');
var middleware = require('./middleware');
var app = express();


app.use(middleware.logger);
//app.use(middleware.requireAuthentication);

app.get('/about',middleware.requireAuthentication, function(req, res){
   res.send('about us -- we are awesome');
});

// testing the  variable
//console.log(__dirname + '/public');

app.use(express.static(__dirname + '/public'));

var PORT =   process.env.PORT ||3000;

app.listen(PORT, function(){
   console.log('server is listening on port: '.green+ PORT);
});