// use module exports to expose middleware

// there are many ways
// this is way 1


//    var middleware = module.exports= {
//        requireAuthentication: function (req, res, next) {
//            console.log('private route hit!');
//            next();
//        },
//        logger: function (req, res, next) {
//            console.log('Request: ' + req.method + ' ' + req.originalUrl + ' ' + new Date().toString());
//            next();
//        }
//    };


// this is way 2

var middleware = module.exports= {
    requireAuthentication: function (req, res, next) {
        console.log('private route hit!');
        next();
    },
    logger: function (req, res, next) {
        console.log('Request: ' + req.method + ' ' + req.originalUrl + ' ' + new Date().toString());
        next();
    }
};

module.exports = middleware;



